## PREREQUISITE

### Source Code
Download the source code from [GitLab](https://git.wur.nl/wsg35306/marina-practical/).

### Conda

The MARINA practical requires conda or anaconda for setting up the Jupyter Notebooks and dependencies. Install from the [website](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html).

## SETUP 

1) Unzip the source code on your machine.
2) Open a terminal e.g.:
    - Anaconda: anaconda prompt
    - Linux: terminal/shell
3) Navigate to the extracted files where you have folders like MARINA, notebooks etc.
4) Run the following command:
```sh
conda env create -f config/<your os>/marina_pract_env.yaml
```

## START

Skip step 1 and 2 if your just did the SETUP steps.

1) Open a terminal
2) Navigate to the source code.

3) Activate the conda env:
```sh
conda activate marina_pract
```
4) Start the jupyter notebook server
```sh
jupyter notebook
```


